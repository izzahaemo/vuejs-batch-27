// Soal 2
// Setelah no.1 berhasil, implementasikan function readBooks yang menggunakan callback di atas namun sekarang menggunakan Promise. Buatlah sebuah file dengan nama promise.js. Tulislah sebuah function dengan nama readBooksPromise yang me-return sebuah promise seperti berikut:

var readBooksPromise = require('./promise.js')

var books = [
    { name: 'LOTR', timeSpent: 3000 },
    { name: 'Fidas', timeSpent: 2000 },
    { name: 'Kalkulus', timeSpent: 4000 },
    { name: 'komik', timeSpent: 1000 }
]
var time = 10000;
readBooksPromise(time, books[0]).then(function (remainingTime) {
    time = remainingTime;
    readBooksPromise(time, books[1]).then(function (remainingTime) {
        time = remainingTime;
        readBooksPromise(time, books[2]).then(function (remainingTime) {
            time = remainingTime;
            readBooksPromise(time, books[3]).then(function (remainingTime) {
                time = remainingTime;
            }).catch(function (error) {
            })
        }).catch(function (error) {
        })
    }).catch(function (error) {
    })
}).catch(function (error) {
})

// Lakukan hal yang sama dengan soal no.1, habiskan waktu selama 10000 ms (10 detik) untuk membaca semua buku dalam array books.!


// *Petunjuk
// Untuk mengerjakan soal di atas , tidak perlu dilooping, cukup panggil satu satu elemen tiap array nya : books[0] , books[1] , dst.
// Gunakan sisa waktu membaca books[0] untuk membaca books[1], kemudaian sisa waktu membaca books[1] untuk membaca books[2], dst.