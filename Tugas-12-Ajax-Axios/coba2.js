var vm = new Vue({
    el: '#app',
    data: {
        blogs: []
    },
    methods: {
        getBlogs: function () {
            const config = {
                method: "get",
                url: "http://demo-api-vue.sanbercloud.com/api/blog",
            }
            axios(config)
                .then((response) => {
                    this.blogs = response.data.blogs
                    console.log(blogs)

                })
                .catch((error) => {
                    console.log(error)
                });
        }
    },
    created() {
        this.getBlogs()
    }

})