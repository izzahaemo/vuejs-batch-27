var vm = new Vue({
    el: '#app',
    data: {
        members: [],
        photoDomain: 'http://demo-api-vue.sanbercloud.com',
        errors: [],
        name: '',
        address: '',
        no_hp: '',
        photo_profile: '',
        deleteMembers: "",
        buttonStatus: 'submit',
        noww: new Date().toLocaleString(),
        idEdit: null,
    },
    methods: {
        validationForm: function () {
            this.errors = []
            if (this.name.length < 5) {
                this.errors.push('Nama minimal 5 karakter')
                alert('Nama minimal 5 karakter')
                this.$refs.name.focus()
            }
            else if (this.address.length < 5) {
                this.errors.push('Alamat minimal 5 karakter')
                alert('Alamat minimal 5 karakter')
                this.$refs.address.focus()
            }
            else if (this.no_hp.length < 5) {
                this.errors.push('No HP minimal 5 karakter')
                alert('No HP minimal 5 karakter')
                this.$refs.no_hp.focus()
            }
        },
        clearForm: function () {
            this.name = ''
            this.address = ''
            this.no_hp = ''
            this.idEdit = null
            this.buttonStatus = 'submit'
        },
        submitForm: function () {
            this.validationForm()
            if (this.errors.length === 0) {

                let formData = new FormData()
                formData.append('name', this.name)
                formData.append('address', this.address)
                formData.append('no_hp', this.no_hp)

                const config = {
                    method: "post",
                    url: "http://demo-api-vue.sanbercloud.com/api/member",
                    data: formData
                }
                axios(config)
                    .then((response) => {
                        this.clearForm()
                        this.getMembers()
                        alert(response.data.message)
                    })
                    .catch((error) => {
                        console.log(error)
                    });
            }
        },
        editMembers: function (member) {
            this.idEdit = member.id
            this.name = member.name
            this.address = member.address
            this.no_hp = member.no_hp
            this.buttonStatus = 'update'
        },
        updateMembers: function (id) {
            this.validationForm()
            if (this.errors.length === 0) {
                let formData = new FormData()
                formData.append('name', this.name)
                formData.append('address', this.address)
                formData.append('no_hp', this.no_hp)
                const config = {
                    method: "post",
                    url: `http://demo-api-vue.sanbercloud.com/api/member/${id}`,
                    params: { _method: "PUT" },
                    data: formData
                }
                axios(config)
                    .then((response) => {
                        this.clearForm()
                        this.getMembers()
                        alert(response.data.message)
                    })
                    .catch((error) => {
                        console.log(error)
                    });
            }
        },
        uploadPhoto: function (member) {
            this.idEdit = member.id
            this.name = member.name
            this.address = member.address
            this.no_hp = member.no_hp
            this.buttonStatus = "upload"
        },
        submitPhoto: function (id) {
            let file = this.$refs.photo.files[0]
            let formData = new FormData()
            formData.append('photo_profile', file)
            const config = {
                method: "post",
                url: `http://demo-api-vue.sanbercloud.com/api/member/${id}/upload-photo-profile`,
                data: formData
            }
            axios(config)
                .then((response) => {
                    this.clearForm()
                    this.getMembers()
                    alert(response.data.message)
                })
                .catch((error) => {
                    console.log(error)
                });
        },
        removeMembers: function (id) {
            deleteMembers = confirm("Hapus User ini? ")
            if (deleteMembers) {
                const config = {
                    method: 'post',
                    url: `http://demo-api-vue.sanbercloud.com/api/member/${id}`,
                    params: { _method: "DELETE" },
                }
                axios(config)
                    .then((response) => {
                        this.getMembers()
                        alert(response.data.message)
                    })
                    .catch((error) => {
                        console.log(error)
                    });
            }
        },
        getMembers: function () {
            const config = {
                method: "get",
                url: "http://demo-api-vue.sanbercloud.com/api/member",
            }
            axios(config)
                .then((response) => {
                    this.members = response.data.members
                    console.log(this.members)
                    console.log(this.noww)
                })
                .catch((error) => {
                    console.log(error)
                });
        },
    },
    created() {
        this.getMembers()
    }

})