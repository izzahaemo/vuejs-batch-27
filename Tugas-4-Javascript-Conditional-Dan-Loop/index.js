//Soal 1
var nilai = 78;
// pilih angka dari 0 sampai 100, misal 75. lalu isi variabel tersebut dengan angka tersebut. lalu buat lah pengkondisian dengan if-elseif dengan kondisi

// nilai >= 85 indeksnya A
// nilai >= 75 dan nilai < 85 indeksnya B
// nilai >= 65 dan nilai < 75 indeksnya c
// nilai >= 55 dan nilai < 65 indeksnya D
// nilai < 55 indeksnya E

if (nilai >= 85) {
    var indeks = "A";
} else if (nilai >= 75 && nilai < 85) {
    var indeks = "B";
} else if (nilai >= 65 && nilai < 75) {
    var indeks = "C";
} else if (nilai >= 55 && nilai < 65) {
    var indeks = "D";
} else if (nilai < 55) {
    var indeks = "E";
}

console.log("Nilai ", nilai, " indeks = ", indeks, "\n");

//soal 2

var tanggal = 09;
var bulan = 9;
var tahun = 1998;
// ganti tanggal ,bulan, dan tahun sesuai dengan tanggal lahir anda dan buatlah switch case pada bulan, lalu muncul kan string nya dengan output seperti ini 22 Juli 2020 (isi di sesuaikan dengan tanggal lahir masing-masing)

switch (bulan) {
    case 1: var namabulan = "Januari"; break;
    case 2: var namabulan = "Februari"; break;
    case 3: var namabulan = "Maret"; break;
    case 4: var namabulan = "April"; break;
    case 5: var namabulan = "Mei"; break;
    case 6: var namabulan = "Juni"; break;
    case 7: var namabulan = "Juli"; break;
    case 8: var namabulan = "Agustus"; break;
    case 9: var namabulan = "September"; break;
    case 10: var namabulan = "Oktober"; break;
    case 11: var namabulan = "November"; break;
    case 12: var namabulan = "Desember"; break;
    default: var namabulan = "Not Found"; break;
}
console.log("bulan= ", bulan);
console.log(tanggal, namabulan, tahun, "\n");

//soal 3
//Kali ini kamu diminta untuk menampilkan sebuah segitiga dengan tanda pagar (#) dengan dimensi tinggi n dan alas n. Looping boleh menggunakan syntax apa pun (while, for, do while).
var n = 10;
console.log("n= ", n);
for (i = 0; i < n; i++) {
    for (z = 0; z <= i; z++) {
        process.stdout.write("#");
    }
    process.stdout.write("\n");
}
process.stdout.write("\n");

//soal 4
//berilah suatu nilai m dengan tipe integer, dan buatlah pengulangan dari 1 sampai dengan m, dan berikan output
var m = 15;
var one = 1;
var two = 2;
var three = 3;
console.log("m = ", m);
for (i = 1; i <= m; i++) {
    if (i == one) {
        console.log(i, " - I love programming");
        var one = one + 3;
    }
    if (i == two) {
        console.log(i, " - I love Javascript");
        var two = two + 3;
    }
    if (i == three) {
        console.log(i, " - I love VueJS");
        for (g = 0; g < three; g++) {
            process.stdout.write("=");
        }
        process.stdout.write("\n");
        var three = three + 3;
    }
}
process.stdout.write("\n");