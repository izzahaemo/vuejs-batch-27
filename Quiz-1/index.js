// 1.
// Judul : Function Penghitung Jumlah Kata
// Buatlah sebuah function dengan nama jumlah_kata() yang menerima sebuah kalimat (string), dan mengembalikan nilai jumlah kata dalam kalimat tersebut.

function jumlah_kata(data) {
    var panjang = data.length;
    var jumlah = 1;
    var onn = 0;
    for (i = 0; i < panjang; i++) {
        if (i != 0 && i != panjang - 1) {
            if (data[i] == " ") {
                jumlah = jumlah + 1;
                onn = 1;
            }
        }
    }
    if (onn == 0) {
        if (panjang > 1) {
            if (data[2] != " ") {
                jumlah = 1;
            }
        }
        else {
            jumlah = 0;
        }
    }
    return jumlah;
}

var kalimat_1 = " Halo nama saya Muhammad Iqbal Mubarok ok";
var kalimat_2 = "Saya Iqbal Bro";


var jumlah_kata1 = jumlah_kata(kalimat_1);
console.log(jumlah_kata1);
var jumlah_kata2 = jumlah_kata(kalimat_2);
console.log(jumlah_kata2);

// 2.
// Judul : Function Penghasil Tanggal Hari Esok
// Buatlah sebuah function dengan nama next_date() yang menerima 3 parameter tanggal, bulan, tahun dan mengembalikan nilai tanggal hari esok dalam bentuk string, dengan contoh input dan otput sebagai berikut.
function next_date(tanggal, bulan, tahun) {
    tanggall = tanggal;
    tahunkabisat = [1988, 1992, 1996, 2000, 2004, 2008, 2012, 2016, 2020, 2024]
    if (bulan == 1) {
        if (tanggal == 31) {
            bulan = bulan + 1;
            tanggal = 1;
        }
    } else if (bulan == 2) {
        var onn = 1;
        for (i = 0; i < 10; i++) {
            if (tahun == tahunkabisat[i]) {
                if (tanggal == 29) {
                    bulan = bulan + 1;
                    tanggal = 1;
                    onn = 2;
                }
            }
        } if (onn == 1) {
            if (tanggal == 28) {
                bulan = bulan + 1;
                tanggal = 1;
            }
        }
    } else if (bulan == 3) {
        if (tanggal == 31) {
            bulan = bulan + 1;
            tanggal = 1;
        }
    } else if (bulan == 4) {
        if (tanggal == 30) {
            bulan = bulan + 1;
            tanggal = 1;
        }
    } else if (bulan == 5) {
        if (tanggal == 31) {
            bulan = bulan + 1;
            tanggal = 1;
        }
    } else if (bulan == 6) {
        if (tanggal == 30) {
            bulan = bulan + 1;
            tanggal = 1;
        }
    } else if (bulan == 7) {
        if (tanggal == 31) {
            bulan = bulan + 1;
            tanggal = 1;
        }
    } else if (bulan == 8) {
        if (tanggal == 31) {
            bulan = bulan + 1;
            tanggal = 1;
        }
    } else if (bulan == 9) {
        if (tanggal == 30) {
            bulan = bulan + 1;
            tanggal = 1;
        }
    } else if (bulan == 10) {
        if (tanggal == 31) {
            bulan = bulan + 1;
            tanggal = 1;
        }
    } else if (bulan == 11) {
        if (tanggal == 30) {
            bulan = bulan + 1;
            tanggal = 1;
        }
    } else if (bulan == 12) {
        if (tanggal == 31) {
            bulan = 1;
            tanggal = 1;
            tahun = tahun + 1;
        }
    }
    if (tanggal == tanggall) {
        tanggal = tanggal + 1;
    }
    switch (bulan) {
        case 1: var namabulan = "Januari"; break;
        case 2: var namabulan = "Februari"; break;
        case 3: var namabulan = "Maret"; break;
        case 4: var namabulan = "April"; break;
        case 5: var namabulan = "Mei"; break;
        case 6: var namabulan = "Juni"; break;
        case 7: var namabulan = "Juli"; break;
        case 8: var namabulan = "Agustus"; break;
        case 9: var namabulan = "September"; break;
        case 10: var namabulan = "Oktober"; break;
        case 11: var namabulan = "November"; break;
        case 12: var namabulan = "Desember"; break;
    }
    console.log(tanggal, namabulan, tahun);
}

var tanggal = 28;
var bulan = 2;
var tahun = 2015;
next_date(tanggal, bulan, tahun);


