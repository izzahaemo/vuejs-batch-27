// Soal 1
// buatlah fungsi menggunakan arrow function luas dan keliling persegi panjang dengan arrow function lalu gunakan let atau const di dalam soal ini

const luas = (a, b) => {
    return a * b;
}

const keliling = (a, b) => {
    return (a * 2) + (2 * b);
}
console.log(luas(3, 4), keliling(5, 6));

// Soal 2
// Ubahlah code di bawah ke dalam arrow function dan object literal es6 yang lebih sederhana

// const newFunction = function literal(firstName, lastName) {
//     return {
//         firstName: firstName,
//         lastName: lastName,
//         fullName: function () {
//             console.log(firstName + " " + lastName)
//         }
//     }
// }

const newFunction = (firstname, lastname) => {
    return {
        fullName: function () {
            console.log(firstname, lastname);
        }
    }
}

//Driver Code 
newFunction("William", "Imoh").fullName()

// Soal 3
// Diberikan sebuah objek sebagai berikut:
const newObject = {
    firstName: "Muhammad",
    lastName: "Iqbal Mubarok",
    address: "Jalan Ranamanyar",
    hobby: "playing football",
}
const { firstName, lastName, address, hobby } = newObject;
console.log(firstName, lastName, address, hobby);

// soal 4
// Kombinasikan dua array berikut menggunakan array spreading ES6
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
//const combined = west.concat(east)

let combined = [...west, ...east];
//Driver Code
console.log(combined)

// soal 5
// sederhanakan string berikut agar menjadi lebih sederhana menggunakan template literals ES6:

const planet = "earth"
const view = "glass"
//var before = 'Lorem ' + view + 'dolor sit amet, ' + 'consectetur adipiscing elit,' + planet 
const before = `Lorem ${view} dolor sit amet, consectetur adipiscing elit ${planet}`;
console.log(before);