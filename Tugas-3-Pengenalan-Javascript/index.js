//soal 1
var pertama = "saya sangat senang hari ini";
var kedua = "belajar javascript itu keren";
//saya senang belajar JAVASRIPT

var saya = pertama.substring(0, 5);
var sangat = pertama.substring(12, 19);
var belajar = kedua.substring(0, 8);
var javascript = kedua.substring(8, 18).toUpperCase();

var hasil1 = saya.concat(sangat).concat(belajar).concat(javascript);
console.log(hasil1);

//soal 2
var kataPertama = "10";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "6";
// ubahlah variabel diatas ke dalam integer dan lakukan operasi matematika semua variabel agar menghasilkan output 24 (integer).
// *catatan :
// 1. gunakan 3 operasi, tidak boleh  lebih atau kurang. contoh : 10 + 2 * 4 + 6
// 2. Boleh menggunakan tanda kurung . contoh : (10 + 2 ) * (4 + 6)

var hasil2 = parseInt(kataKetiga) * parseInt(kataKedua) + parseInt(kataPertama) + parseInt(kataKeempat);
console.log(hasil2);

//soal 3
var kalimat = 'wah javascript itu keren sekali';

var kataPertama = kalimat.substring(0, 3);
var kataKedua = kalimat.substring(4, 14);
var kataKetiga = kalimat.substring(15, 18);
var kataKeempat = kalimat.substring(19, 24);
var kataKelima = kalimat.substring(25, 31);

console.log('Kata Pertama: ' + kataPertama);
console.log('Kata Kedua: ' + kataKedua);
console.log('Kata Ketiga: ' + kataKetiga);
console.log('Kata Keempat: ' + kataKeempat);
console.log('Kata Kelima: ' + kataKelima);
// selesaikan variabel yang belum diisi dan hasilkan output seperti berikut:

// Kata Pertama: wah
// Kata Kedua: javascript
// Kata Ketiga: itu
// Kata Keempat: keren
// Kata Kelima: sekali