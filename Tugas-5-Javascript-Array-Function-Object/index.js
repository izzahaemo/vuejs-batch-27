//soal 1
var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];
//urutkan array di atas dan tampilkan data seperti output di bawah ini (dengan menggunakan loop):

daftarHewan.sort();
for (i = 0; i < 5; i++) {
    console.log(daftarHewan[i]);
}
process.stdout.write("\n");

//soal2
//Tulislah sebuah function dengan nama introduce() yang memproses paramater yang dikirim menjadi sebuah kalimat perkenalan seperti berikut: "Nama saya [name], umur saya [age] tahun, alamat saya di [address], dan saya punya hobby yaitu [hobby]!"
function introduce(data) {
    var data = { name: "Izzah", age: 22, address: "Jombor Indah", hobby: "Gaming" };
    return data;
}

var perkenalan = introduce("data");
// Menampilkan "Nama saya John, umur saya 30 tahun, alamat saya di Jalan Pelesiran, dan saya punya hobby yaitu Gaming" 
console.log("Nama saya " + perkenalan.name + " umur saya " + perkenalan.age + " tahun, alamat saya di " + perkenalan.address + ", dan saya punya hobby yaitu " + perkenalan.hobby, "\n");

//soal3 
//Tulislah sebuah function dengan nama hitung_huruf_vokal() yang menerima parameter sebuah string, kemudian memproses tersebut sehingga menghasilkan total jumlah huruf vokal dalam string tersebut.
function hitung_huruf_vokal(data) {
    var panjang = data.length;
    var vokal = 0;
    for (i = 0; i < panjang; i++) {
        if (data[i] == "a" || data[i] == 'e' || data[i] == 'i' || data[i] == 'o' || data[i] == 'u') {
            vokal = vokal + 1;
        } else if (data[i] == "A" || data[i] == 'E' || data[i] == 'I' || data[i] == 'O' || data[i] == 'U') {
            vokal = vokal + 1;
        }
    }
    return vokal;

}
var hitung_1 = hitung_huruf_vokal("Muhammad");
var hitung_2 = hitung_huruf_vokal("Iqbal");

console.log(hitung_1, hitung_2, "\n") // 3 2

//soal 4
//Buatlah sebuah function dengan nama hitung() yang menerima parameter sebuah integer dan mengembalikan nilai sebuah integer, dengan contoh input dan otput sebagai berikut.
function hitung(angka) {
    var isi = 2 * angka - 2;
    return isi;
}
console.log(hitung(0)); // -2
console.log(hitung(1)); // 0
console.log(hitung(2)); // 2
console.log(hitung(3)); // 4
console.log(hitung(5)); // 8
console.log(hitung(6)); // 10